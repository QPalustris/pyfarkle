''' Terminal-based Farkle Game '''

import os
import pickle
import time
import random
from collections import Counter
from itertools import combinations

import appdirs
from tacotui import screen, color, theme, widgets
from tacotui.getch import getch

import fark_table


statspath = os.path.join(appdirs.user_data_dir('pyfarkle'), 'scores.dat')
playernames = ['Super Sally', 'Happy Harry', 'Maniacal Matt']


def settheme(name):
    ''' Set color theme, override some defaults from tacotui '''
    theme.settheme(name)
    if name == 'blue':
        theme.SELECT = color.BRIGHTWHITE
        theme.MSG = color.BRIGHTYELLOW
        theme.NAME = color.BRIGHTYELLOW
    elif name == 'sand':
        theme.HLIGHT = color.WHITE_BGBLUE
        theme.SELECT = color.CYAN
    else:
        theme.SELECT = color.BRIGHTYELLOW
        theme.HSELECT = color.BRIGHTYELLOW_BGBLUE


# Screen locations
PLAYERLIST = 2, 1
TURNSCORE = 2, 8
ROLL = 2, 10
THROWSCORE = 18, ROLL[1]
MESSAGE = THROWSCORE
ROLLBUTTON = 4, ROLL[1] + 2
BANKBUTTON = 12, ROLLBUTTON[1]
DIEW = 1  # Set DIEW, DIEH as parameters in case we want to make multichar dice at some point
DIEH = 1


def printplayers(players, current):
    ''' Print the player names and scores '''
    screen.printat('Players\n────────', *PLAYERLIST)
    for y, p in enumerate(players):
        clr = theme.NAME if y == current else theme.NORM
        screen.printat(f'{clr}{p.name}:  {p.totalscore}{theme.NORM}', PLAYERLIST[0], PLAYERLIST[1]+y+2)


def printdie(value, x, y, select=False, hlite=False):
    screen.setxy(x, y)
    ch = {1: '\u2780',
          2: '\u2781',
          3: '\u2782',
          4: '\u2783',
          5: '\u2784',
          6: '\u2785'}[value]
    if hlite and select:
        screen.sprint(f'{theme.HSELECT}')
    elif hlite:
        screen.sprint(f'{theme.HLIGHT}')
    elif select:
        screen.sprint(f'{theme.SELECT}')
        ch = {1: '\u2776',
              2: '\u2777',
              3: '\u2778',
              4: '\u2779',
              5: '\u277A',
              6: '\u277B'}[value]

    # Could use dice unicode characters, but they're hard to read
    #    ch = {1: '\U00002680',
    #          2: '\U00002681',
    #          3: '\U00002682',
    #          4: '\U00002683',
    #          5: '\U00002684',
    #          6: '\U00002685'}

    screen.sprint(ch)
    screen.sprint(f'{theme.NORM}')


def printroll(roll, cursor, keepidx):
    ''' Print the dice roll, with cursor selection/highlight '''
    screen.setxy(*ROLL)
    screen.clearline()
    for i, r in enumerate(roll):
        printdie(r, x=ROLL[0]+(DIEW+1)*i, y=ROLL[1], select=(i in keepidx), hlite=(i == cursor))


def printturnscore(turnscore):
    ''' Print the score for the current turn '''
    screen.setxy(*TURNSCORE)
    screen.clearline()
    screen.sprint(f'Turn score: {turnscore}')


def printholdscore(points):
    ''' Print the score of held dice from this throw '''
    screen.setxy(*THROWSCORE)
    screen.clearline()
    screen.sprint(f'\u2192 {theme.SELECT}{points}{theme.NORM}')


def printbuttons(selroll, selbank, bankenable=True):
    ''' Print roll and bank buttons '''
    if not bankenable:
        screen.printat(f'{color.rgb(90,90,90)}BANK?{theme.NORM}', *BANKBUTTON)
    elif selbank:
        screen.printat(f'{theme.HLIGHT}BANK?{theme.NORM}', *BANKBUTTON)
    else:
        screen.printat('BANK?', *BANKBUTTON)
    if selroll:
        screen.printat(f'{theme.HLIGHT}ROLL?{theme.NORM}', *ROLLBUTTON)
    else:
        screen.printat(f'ROLL?', *ROLLBUTTON)


def printmessage(message, timeout=2):
    ''' Print a message '''
    screen.printat(f'{theme.MSG}{message}{theme.NORM}', *MESSAGE)
    time.sleep(timeout)


def printstats(stats):
    ''' Print game statistics '''
    print(f'Total games: {stats["total"]}')
    print(f'Total wins: {stats["wins"]}')
    print(f'Win rate: {stats["wins"]/stats["total"]*100:.1f}%')
    print(f'Highest score: {stats["highscore"]}')
    print()
    print(f'{playernames[0]} games: {stats["p1games"]}')
    print(f' Wins: {stats["p1wins"]}')
    try:
        print(f' Win rate: {stats["p1wins"]/stats["p1games"]*100:.1f}%')
    except ZeroDivisionError:
        print(' Win rate: 0%')
    print()
    print(f'{playernames[1]} games: {stats["p2games"]}')
    print(f' Wins: {stats["p2wins"]}')
    try:
        print(f' Win rate: {stats["p2wins"]/stats["p2games"]*100:.1f}%')
    except ZeroDivisionError:
        print(' Win rate: 0%')
    print()
    print(f'{playernames[2]} games: {stats["p3games"]}')
    print(f' Wins: {stats["p3wins"]}')
    try:
        print(f' Win rate: {stats["p3wins"]/stats["p3games"]*100:.1f}%')
    except ZeroDivisionError:
        print(' Win rate: 0%')
    
    
    
class Player(object):
    ''' Human player '''
    def __init__(self, name, threshold=500):
        self.name = name
        self.totalscore = 0
        self.threshold = threshold

        self.holdscore = 0
        self.turnscore = 0
        self.inplay = 6

    def turn(self):
        ''' One turn '''
        self.inplay = 6
        while True:
            self.keepidx = []
            self.doroll()
            if self.canscore():
                printturnscore(self.turnscore)
                printroll(self.roll, cursor=0, keepidx=[])
                if self.select_keeps() > 0:
                    self.totalscore += self.turnscore
                    self.turnscore = 0
                    self.threshold = 1
                    break
            else:
                printmessage('FARKLE')
                self.turnscore = 0
                self.holdscore = 0
                break

    def doroll(self):
        ''' Roll the dice '''
        self.roll = [random.randint(1, 6) for i in range(self.inplay)]
        printroll(self.roll, cursor=None, keepidx=[])

    def select_keeps(self):
        ''' Select which dice to score '''
        cursor = 0
        while True:
            points = self.getrollscore()
            printroll(self.roll, cursor, self.keepidx)
            printholdscore(points)
            bankenable = points + self.turnscore >= self.threshold
            printbuttons(cursor == len(self.roll), cursor == len(self.roll)+1, bankenable)

            key = getch()
            if key == 'left' and cursor > 0:
                cursor -= 1
            elif key == 'right' and (cursor < len(self.roll) or (cursor < len(self.roll)+1 and bankenable)):
                cursor += 1
            elif key == 'down' and (cursor < len(self.roll)):
                cursor = len(self.roll)
            elif key == 'up' and (cursor >= len(self.roll)):
                cursor = 0
            elif key in [' ', 'enter']:
                if cursor == len(self.roll):  # ROLL
                    if points > 0:
                        self.turnscore += points
                        self.inplay = self.inplay - len(self.keepidx)
                        if self.inplay == 0:
                            self.inplay = 6  # New round, keep going
                        self.keepidx = []
                        return 0
                    else:
                        printmessage('Try again')
                        self.keepidx = []
                        screen.setxy(*MESSAGE)
                        screen.clearline()

                elif cursor == len(self.roll) + 1 and bankenable:  # BANK
                    self.turnscore += points
                    return self.turnscore

                else:
                    self.hold(cursor)

    def getrollscore(self):
        ''' Get the score for the currently held dice '''
        roll = [self.roll[k] for k in self.keepidx]
        c = Counter(roll)

        if len(c) == 6:
            return 1500  # Straight
        elif len(c) == 3 and set(c.values()) == {2}:
            return 750   # 3-pairs

        # Count up 3+ of a kind, 1's and 5's
        score = 0
        for val, count in c.most_common(n=len(roll)):
            add = 0
            if count > 2 and val == 1:
                add = 1000 * (count-2)
            elif count > 2:
                add = val * 100 * (count-2)
            elif val == 1:
                add = 100 * count
            elif val == 5:
                add = 50 * count
            if add > 0:   # Remove scored dice
                roll = [r for r in roll if r != val]
                score += add
        if len(roll) > 0:
            return 0
        return score

    def canscore(self):
        ''' Determine if the roll can score, or is farkle '''
        c = Counter(self.roll)
        if (1 in self.roll or 5 in self.roll   # 1's or 5's always score
           or c.most_common(1)[0][1] > 2      # 3+ of a kind can score
           or len(c) == 6):                   # Straight can score
            return True
        return False

    def hold(self, idx):
        ''' Hold die at index '''
        if idx in self.keepidx:
            self.keepidx.remove(idx)
        else:
            self.keepidx.append(idx)


class PlayerAI(Player):
    ''' Computer player. Same as human but different process for holding dice. This player banks
        whenever 2 or fewer die remain to roll, or if this turn released the opening threshold.
    '''
    def ai_pick_keeps(self):
        combos = []
        for i in range(len(self.roll)):
            combos.extend(combinations(list(range(len(self.roll))), i+1))
        maxscore = -1
        seldice = []
        for combo in combos:
            self.keepidx = combo
            points = self.getrollscore()
            if points > maxscore:
                maxscore = points
                seldice = combo

        n = len(self.roll) - len(seldice)
        canbank = maxscore + self.turnscore >= self.threshold
        bank = (canbank and 
                ((n > 0 and n <= 2) or             # Bank when <3 die left
                 (n > 0 and self.threshold > 1)))  # Or if we can hit threshold
        self.keepidx = []        
        return seldice, bank
                      
    def select_keeps(self):
        ''' Select which dice to hold '''
        # Decide which dice to keep and whether to bank
        seldice, bank = self.ai_pick_keeps()
        
        # Animate the selection
        printbuttons(selroll=False, selbank=False, bankenable=False)
        printroll(self.roll, None, self.keepidx)
        time.sleep(1.0)
        for i in seldice:
            self.hold(i)
            points = self.getrollscore()
            printroll(self.roll, i, self.keepidx)
            printholdscore(points)
            bankenable = points + self.turnscore >= self.threshold
            printbuttons(selroll=False, selbank=False, bankenable=bankenable)
            time.sleep(1.0)

        bankenable = points + self.turnscore >= self.threshold
        printroll(self.roll, None, self.keepidx)  # Clear cursor from roll
        if bank:
            printbuttons(selroll=False, selbank=True, bankenable=bankenable)
            self.turnscore += points
            time.sleep(2.0)
            return self.turnscore
        else:
            printbuttons(selroll=True, selbank=False, bankenable=bankenable)
            time.sleep(1.0)
            self.turnscore += points
            self.inplay = self.inplay - len(self.keepidx)
            if self.inplay == 0:
                self.inplay = 6
            return 0


class Player2(PlayerAI):
    ''' This player banks if he has more than 750 points regardless of die remaining '''
    minturn = 750
    def ai_pick_keeps(self):
        combos = []
        for i in range(len(self.roll)):
            combos.extend(combinations(list(range(len(self.roll))), i+1))

        maxscore = -1
        bestcombo = None
        for combo in combos:
            self.keepidx = combo
            t = self.getrollscore()
            if t > 0 and t > maxscore:
                maxscore = t
                bestcombo = combo
        n = len(self.roll) - len(bestcombo)
        bank = (maxscore + self.turnscore >= self.threshold and n != 0 and maxscore+self.turnscore > self.minturn)
        self.keepidx = []
        return bestcombo, bank
       

class Player3(PlayerAI):
    ''' This player is based on the optimal strategy published by Maniacal Matt at
        http://www.mattbusche.org/blog/article/zilch/
        
        Using the table matching the farkle rules used here.
        http://www.mattbusche.org/projects/farkle/strategy.php?set1=100,200,1000,2000,3000,4000&set2=0,0,200,400,600,800&set3=0,0,300,600,900,1200&set4=0,0,400,800,1200,1600&set5=50,100,500,1000,1500,2000&set6=0,0,600,1200,1800,2400&straight=1500&threepair=750&flexpairs=false&twotriplet=0&nothing=0&penalty=0&minbank=0
    '''
    def ai_pick_keeps(self):
        combos = []
        for i in range(len(self.roll)):
            combos.extend(combinations(list(range(len(self.roll))), i+1))
        best = 0
        bestn = None
        bestcombo = None
        bank = None
        for combo in combos:
            self.keepidx = combo
            t = self.getrollscore()
            if t > 0:
                n = len(self.roll) - len(combo)
                n = 6 if n == 0 else n
                w = fark_table.T[(t+self.turnscore)//50][6-n]
                if w and w > best:
                    bestcombo = combo
                    best = w
                    bestn = n
                    bank = (t + self.turnscore >= self.threshold) and (t+self.turnscore >= fark_table.bankmin[6-n])
                elif w == best and n > bestn:
                    bestcombo = combo
                    bestn = n
                    bank = (t + self.turnscore >= self.threshold) and (t+self.turnscore >= fark_table.bankmin[6-n])
        self.keepidx = []
        return bestcombo, bank


def load_stats():
    ''' Load statistics from file '''
    if not os.path.exists(statspath):
        stats = {
            'total': 0,
            'wins': 0,
            'highscore': 0,
            'p1games': 0,
            'p1wins': 0,
            'p2games': 0,
            'p2wins': 0,
            'p3games': 0,
            'p3wins': 0,}
    else:
        with open(statspath, 'rb') as f:
            stats = pickle.load(f)
    return stats


def save_stats(stats):
    ''' Save statistics to file '''
    os.makedirs(os.path.dirname(statspath), exist_ok=True)
    with open(statspath, 'wb') as f:
        pickle.dump(stats, f)


def clear_stats():
    ''' Clear the statistics file '''
    os.remove(statspath)
        
    
def game(human='Human', comp='Computer', winscore=10000, threshold=500):
    ''' Play the game '''
    players = [Player(human, threshold=threshold)]
    if comp == playernames[0]:
        players.append(PlayerAI(comp, threshold=threshold))
    elif comp == playernames[1]:
        players.append(Player2(comp, threshold=threshold))
    else:
        players.append(Player3(comp, threshold=threshold))

    p = 0
    while True:
        screen.clear()
        printplayers(players, p)
        players[p].turn()

        if players[p].totalscore >= winscore:
            printplayers(players, p)
            screen.printat(f'{theme.MSG}{players[p].name} Wins!{theme.NORM}', *MESSAGE)
            
            stats = load_stats()
            stats['total'] += 1
            if p == 0:
                stats['wins'] += 1
                stats['highscore'] = max(stats['highscore'], players[p].totalscore)

            if comp == playernames[0]:
                stats['p1games'] += 1
                if p == 0:
                    stats['p1wins'] += 1
            elif comp == playernames[1]:
                stats['p2games'] += 1
                if p == 0:
                    stats['p2wins'] += 1
            else:
                stats['p3games'] += 1
                if p == 0:
                    stats['p3wins'] += 1
            
            save_stats(stats)                    
            getch()            
            screen.clear()
            printstats(stats)
            getch()            
            break
        p = (p+1) % len(players)


if __name__ == '__main__':
    screen.hide_cursor()
    screen.clear()
    opts = widgets.form('Farkle Options',
                        ('Your name', str),
                        ('Computer player', playernames),
                        ('Points to win', int, 10000),
                        ('Opening threshold', int, 500),
                        ('Color theme', ['Normal', 'Blue', 'Sand']))

    settheme(opts['Color theme'].lower())
    game(human=opts['Your name'],
         comp=opts['Computer player'],
         winscore=opts['Points to win'],
         threshold=opts['Opening threshold'])
